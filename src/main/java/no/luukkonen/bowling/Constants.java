package no.luukkonen.bowling;

public final class Constants {
    private Constants() {}
    /**
     * Number of frames (excluding bonus throws) in a game of bowling
     */
    public static final int NUMBER_OF_FRAMES = 10;
    /**
     * Number of pins in bowling
     */
    public static final int NUMBER_OF_PINS = 10;
}
