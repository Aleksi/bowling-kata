package no.luukkonen.bowling;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Represents a single bowling frame.
 * <p>
 * Normal frames can either have two throws with scores, one throw with a score and the second knocking down all pins
 * (a spare), or a single throw knocking down all pins (a strike).
 * Bonus frames result from the final frame being either a spare or a strike.
 * Each bonus frame consists of a single throw, so if the final frame is a strike, two bonus throws are given,
 * but they are represented as two frames, each with one throw.
 */
public final class BowlingFrame {
    private final BowlingFrameType type;
    private final int pinsFirst;
    private final Integer pinsSecond;

    /**
     * @return The type of bowling frame
     */
    public BowlingFrameType getType() {
        return type;
    }

    /**
     * @return The number of pins knocked down on the first throw
     */
    public int getPinsFirstThrow() {
        return pinsFirst;
    }

    /**
     * @return The total number of pins knocked down
     */
    public int getTotalPins() {
        if (pinsSecond == null) {
            return pinsFirst;
        } else {
            return pinsFirst + pinsSecond;
        }
    }

    /**
     * Construct a new BowlingFrame from a string with maximally two throws
     * @param frame A string of length 1 or 2 representing the throws
     * @return The bowling frame
     */
    public static BowlingFrame fromString(final String frame) {
        if (!isValidFrame(frame)) {
            throw new IllegalArgumentException("Invalid frame given: " + frame);
        }

        if (frame.substring(0, 1).equalsIgnoreCase("x")) {
            return fromStrike();
        } else if (frame.charAt(1) == '/') {
            return fromSpare(Integer.parseInt(frame.substring(0, 1)));
        } else {
            // Neither of these should throw as we have called isValidFrame above
            int first = rollToInt(frame.substring(0, 1)).orElseThrow();
            int second = rollToInt(frame.substring(1, 2)).orElseThrow();
            return normal(first, second);
        }
    }

    /**
     * Construct a new BowlingFrame that represents a bonus throw
     * @param frame A string of length 1 representing the throw
     * @return The bowling frame
     */
    public static BowlingFrame fromBonus(final String frame) {
        if (frame == null || frame.length() != 1) {
            throw new IllegalArgumentException("Invalid bonus frame: " + frame);
        }

        if (frame.substring(0, 1).equalsIgnoreCase("x")) {
            return new BowlingFrame(BowlingFrameType.Strike, Constants.NUMBER_OF_PINS);
        } else {
            try {
                int score = rollToInt(frame.substring(0, 1)).orElseThrow();
                return new BowlingFrame(BowlingFrameType.Bonus, score);
            } catch (NoSuchElementException ex) {
                throw new IllegalArgumentException("Invalid bonus frame: " + frame);
            }
        }
    }

    private BowlingFrame(final BowlingFrameType type, final int pinsFirst) {
        this.type = type;
        this.pinsFirst = pinsFirst;
        this.pinsSecond = null;
    }

    private BowlingFrame(final BowlingFrameType type, final int pinsFirst, final int pinsSecond) {
        this.type = type;
        this.pinsFirst = pinsFirst;
        this.pinsSecond = pinsSecond;
    }

    private static BowlingFrame fromStrike() {
        return new BowlingFrame(BowlingFrameType.Strike, Constants.NUMBER_OF_PINS);
    }

    private static BowlingFrame fromSpare(int scoreFirst) {
        return new BowlingFrame(BowlingFrameType.Spare, scoreFirst, Constants.NUMBER_OF_PINS - scoreFirst);
    }

    private static BowlingFrame normal(int scoreFirst, int scoreSecond) {
        return new BowlingFrame(BowlingFrameType.Normal, scoreFirst, scoreSecond);
    }

    private static Optional<Integer> rollToInt(final String rollScore) {
        if (rollScore.equals("-")) {
            return Optional.of(0);
        }

        try {
            return Optional.of(Integer.parseInt(rollScore));
        } catch (NumberFormatException ex) {
            return Optional.empty();
        }
    }

    private static boolean isValidFrame(final String frame) {
        if (frame == null) {
            return false;
        }

        if (frame.length() == 1 && frame.equalsIgnoreCase("x")) {
            // Single length frames that are not bonus frames must be strikes
            return true;
        }

        if (frame.length() == 2) {
            final String first = frame.substring(0, 1);
            final String second = frame.substring(1, 2);
            if (second.equals("/")) {
                return true;
            } else {
                try {
                    final int firstScore = rollToInt(first).orElseThrow();
                    final int secondScore = rollToInt(second).orElseThrow();
                    final int sum = firstScore + secondScore;
                    return sum <= Constants.NUMBER_OF_PINS;
                } catch (NoSuchElementException ex) {
                    return false;
                }
            }
        }

        return false;
    }
}
