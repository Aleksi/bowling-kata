package no.luukkonen.bowling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A class that can calculate scores for a bowling game
 */
public final class BowlingScorer {
    /**
     * No dependencies and no state means we can make the class 'static' and the constructor private.
     * In Kotlin this would be an 'object' (singleton instance) instead of a class.
     */
    private BowlingScorer() {
    }

    /**
     * Score a string representing a bowling game.
     * <p>
     * In the string given each frame consists of either two numbers representing the number of pins knocked down
     * in each of the two rolls, or one number and a / representing a spare, or a single x representing a strike.
     * Frames should be separated by a space. Bonus throws when the final frame is a spare or strike should be passed as
     * separate frames.
     *
     * @param resultsString The string of frames representing the game
     * @return The score of the given game
     * @throws IllegalArgumentException If the frames given in resultsString do not represent a valid game of bowling
     */
    public static int scoreGame(final String resultsString) {
        final var stringFrames = resultsString.split("\\s");
        final var frames = framesFromStringArray(stringFrames);
        int totalScore = 0;

        if (!validateFrames(frames)) {
            throw new IllegalArgumentException("Invalid number of frames in game");
        }

        for (int i = 0; i < Constants.NUMBER_OF_FRAMES; ++i) {
            final var currentFrame = frames.get(i);
            switch (currentFrame.getType()) {
                case Normal:
                    totalScore += currentFrame.getTotalPins();
                    break;
                case Spare:
                    totalScore += currentFrame.getTotalPins() + frames.get(i + 1).getPinsFirstThrow();
                    break;
                case Strike:
                    final var nextTwoFrames = frames.subList(i + 1, i + 3);
                    totalScore += Constants.NUMBER_OF_PINS + calculateStrikeBonus(nextTwoFrames);
                    break;
            }
        }

        return totalScore;
    }

    /**
     * Validate that a list of bowling frames represents a valid game.
     *
     * @param frames The list of frames to validate
     * @return true if the list of frames represents a valid game, false otherwise
     */
    private static boolean validateFrames(final List<BowlingFrame> frames) {
        if (frames.size() == Constants.NUMBER_OF_FRAMES) {
            return true;
        }

        if (frames.size() == (Constants.NUMBER_OF_FRAMES + 1)
                && frames.get(Constants.NUMBER_OF_FRAMES - 1).getType() == BowlingFrameType.Spare) {
            return true;
        }

        return frames.size() == (Constants.NUMBER_OF_FRAMES + 2)
                && frames.get(Constants.NUMBER_OF_FRAMES - 1).getType() == BowlingFrameType.Strike;
    }

    private static int calculateStrikeBonus(final List<BowlingFrame> subsequentFrames) {
        final var first = subsequentFrames.get(0);
        final var second = subsequentFrames.get(1);

        if (first.getType() == BowlingFrameType.Bonus) {
            // If first is bonus frame, second must be as well
            return first.getTotalPins() + second.getTotalPins();
        }

        if (first.getType() == BowlingFrameType.Strike) {
            // If first is strike, we must count the first roll of the second frame as well
            return Constants.NUMBER_OF_PINS + second.getPinsFirstThrow();
        } else {
            return first.getTotalPins();
        }
    }

    private static List<BowlingFrame> framesFromStringArray(final String[] stringFrames) {
        final List<BowlingFrame> frames = new ArrayList<>(stringFrames.length);
        for (int i = 0; i < Constants.NUMBER_OF_FRAMES; ++i) {
            frames.add(BowlingFrame.fromString(stringFrames[i]));
        }
        for (int i = 10; i < stringFrames.length; ++i) {
            frames.add(BowlingFrame.fromBonus(stringFrames[i]));
        }
        return Collections.unmodifiableList(frames);
    }
}
