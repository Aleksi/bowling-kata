package no.luukkonen.bowling;

public enum BowlingFrameType {
    /**
     * Frame with two throws, each knocking down some amount of pins
     */
    Normal,
    /**
     * Frame where the all pins are knocked down with the second throw of the frame
     */
    Spare,
    /**
     * Frame where all the pins are knocked down on the first throw of the frame
     */
    Strike,
    /**
     * Bonus throws resulting from the last frame being either a spare or a strike
     */
    Bonus
}
