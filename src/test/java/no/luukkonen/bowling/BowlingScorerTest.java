package no.luukkonen.bowling;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BowlingScorerTest {
    @Test
    void scoreGame_withAllStrikes() {
        final String game = "X X X X X X X X X X X X";
        final int score = BowlingScorer.scoreGame(game);
        assertEquals(300, score);
    }

    @Test
    void scoreGame_withAllStrikesFirstBonusMiss() {
        final String game = "X X X X X X X X X X - X";
        final int score = BowlingScorer.scoreGame(game);
        assertEquals(280, score);
    }

    @Test
    void scoreGame_withAllSpares() {
        final String game = "5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5";
        final int score = BowlingScorer.scoreGame(game);
        assertEquals(150, score);
    }

    @Test
    void scoreGame_withAllNines() {
        final String game = "9- 9- 9- 9- 9- 9- 9- 9- 9- 9-";
        final int score = BowlingScorer.scoreGame(game);
        assertEquals(90, score);
    }

    @Test
    void scoreGame_withAllNormalFrames() {
        final String game = "51 51 51 51 51 51 51 51 51 51";
        final int score = BowlingScorer.scoreGame(game);
        assertEquals(6 * 10, score);
    }

    @Test
    void scoreGame_withNormalBonusGames() {
        final String game = "x x x x x x x x x x 3 4";
        final int score = BowlingScorer.scoreGame(game);

        // 8 strikes with strikes after them (30*8)
        // plus one strike with a bonus throw of a strike and 3
        // plus one strike with bonus throws of 3 and 4
        // 280 score validated with https://www.bowlinggenius.com/
        final int expectedScore = (30 * 8) + (10 + 10 + 3) + (10 + 3 + 4);

        assertEquals(expectedScore, score);
    }

    @Test
    void scoreGame_withStrikeAndSpare() {
        final String game = "x 34 x 3/ x 34 -- -- -- --";
        final int score = BowlingScorer.scoreGame(game);

        // Strike with 3 + 4 next throws
        // 7 normal throw (3+4)
        // Strike + 3 + 7
        // Spare (3 + 7) + strike (10) next throw
        // Strike + (3 + 4) next throws
        // 3 + 4 normal frame
        // Validated with https://www.bowlinggenius.com/
        final int expectedScore = (10+7)+(7)+(10+10)+(3+7+10)+(10+7)+7;
        assertEquals(expectedScore, score);
    }

    @Test
    void scoreGame_withStrikeBonus() {
        final String game = "x 34 34 34 34 34 45 34 45 12";
        final int score = BowlingScorer.scoreGame(game);
        final int expectedScore = (10 + 7) + 7 + 7 + 7 + 7 + 7 + 9 + 7 + 9 + 3;
        assertEquals(expectedScore, score);
    }

    @Test
    void scoreGame_withTooManyStrikes_fails() {
        final String game = "X X X X X X X X X X X X X";
        Assertions.assertThrows(IllegalArgumentException.class, () -> BowlingScorer.scoreGame(game));
    }

    @Test
    void scoreGame_withTooManyFrames_fails() {
        final String game = "12 12 12 12 12 12 12 12 12 12 12";
        Assertions.assertThrows(IllegalArgumentException.class, () -> BowlingScorer.scoreGame(game));
    }

    @Test
    void scoreGame_withSpareAndTwoBonusFrames_fails() {
        final String game = "X X X X X X X X X X 1/ 1 1";
        Assertions.assertThrows(IllegalArgumentException.class, () -> BowlingScorer.scoreGame(game));
    }

    @Test
    void scoreGame_withFrameScoreTooHigh_fails() {
        final String game = "74 51 51 51 51 51 51 51 51 51";
        Assertions.assertThrows(IllegalArgumentException.class, () -> BowlingScorer.scoreGame(game));
    }

    @Test
    void scoreGame_withInvalidStrikeFrame_fails() {
        final String game = "11 Y X X X X X X X X X X";
        Assertions.assertThrows(IllegalArgumentException.class, () -> BowlingScorer.scoreGame(game));
    }

    @Test
    void scoreGame_withInvalidFrame_fails() {
        final String game = "11 1X X X X X X X X X X X";
        Assertions.assertThrows(IllegalArgumentException.class, () -> BowlingScorer.scoreGame(game));
    }

    @Test
    void scoreGame_withInvalidBonusFrame_fails() {
        final String game = "X X X X X X X X X X X Y X";
        Assertions.assertThrows(IllegalArgumentException.class, () -> BowlingScorer.scoreGame(game));
    }

    @Test
    void scoreGame_withSemifinalStrikeAndNoBonus() {
        final String game = "x x x x x x x x x 51";
        final int score = BowlingScorer.scoreGame(game);
        assertEquals(257, score);
    }
}
